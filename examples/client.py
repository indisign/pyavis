'''
Created on 04.03.2013

@author: berre
'''

from avis.connectors import ElvinConnector
from avis.msg import ConfConn


def receivedMessage(message):
    print("EVENT!! Received message: " + str(message.name()))


c = ElvinConnector()
c.onMessageReceived += receivedMessage;
c.connect("127.0.0.1", 5005)

print("Connected now sending!")

message = ConfConn()
c.sendMessage(message)
