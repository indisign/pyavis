'''
Created on 04.03.2013

@author: berre
'''

import io
import socket

from avis import protocol
from avis.msg import ConfConn

TCP_IP = '127.0.0.1'
TCP_PORT = 5005
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while 1:

    print('Waiting for connection...')

    try:

        conn, addr = s.accept()
        print('Connection address:', addr)
        while 1:
            data = conn.recv(BUFFER_SIZE)

            if (len(data) > 3):
                buf = io.BytesIO()
                buf.write(data)
                buf.seek(0, io.SEEK_SET)
                message = protocol.decodeMessage(buf)

                print("received message: ", message.name())

                sMsg = ConfConn()

                buf = io.BytesIO()
                protocol.encodeMessage(sMsg, buf)
                conn.send(buf.getbuffer())

                conn.close()
                break
    except Exception as ex:
        print("decode error: " + ex.strerror)
