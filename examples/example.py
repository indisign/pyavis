from threading import Event, Timer

from avis.client.elvin import Elvin
from avis.uri import ElvinUri

evt = Event()
elvin = Elvin()
uri = ElvinUri("elvin://localhost")


def closed(reason, msg, error):
    print("Closed: " + msg)

    if (reason == Elvin.CLOSE_REASON_ROUTER_SHUTDOWN_UNEXPECTEDLY):
        print("Try to reconnect in 5 seconds...")
        Timer(5, tryConnect).start()


def tryConnect():
    if not elvin.isConnected():

        print("Connecting...")
        elvin.connect(uri)

        if elvin.isConnected():
            print("Connected!")

            # subscribe & notify
            elvin.subscribe("string (Greeting)")
            # elvin.sendNotification(Notification({"Greeting": "How are you?"}))
        else:
            print("Connection failed. Retrying in 5 seconds")
            Timer(5, tryConnect).start()

    else:
        print("Already connected")


def notificationReceived(sb, ntfy, secure):
    print("received notification: ")
    print(str(ntfy))
    # evt.set()


elvin.onNotificationReceived += notificationReceived
elvin.onClosed += closed

tryConnect()

evt.wait()

elvin.close()
