'''
Created on 01.03.2013

@author: Rene
'''


class LegacyConnectionOptions:
    '''
    Defines legacy client connection option compatibility. Provides a
    two-way mapping between old-style connection options and the new
    scheme defined in the 4.0 client specification.

    Compatibility:
     Standard Name               | Compatibility Name
     ----------------------------+------------------------------------
     Attribute.Max-Count         | router.attribute.max-count
     Attribute.Name.Max-Length   | router.attribute.name.max-length
     Attribute.Opaque.Max-Length | router.attribute.opaque.max-length
     Attribute.String.Max-Length | router.attribute.string.max-length
     Packet.Max-Length           | router.packet.max-length
     Receive-Queue.Drop-Policy   | router.recv-queue.drop-policy
     Receive-Queue.Max-Length    | router.recv-queue.max-length
     Send-Queue.Drop-Policy      | router.send-queue.drop-policy
     Send-Queue.Max-Length       | router.send-queue.max-length
     Subscription.Max-Count      | router.subscription.max-count
     Subscription.Max-Length     | router.subscription.max-length
     Supported-Key-Schemes       | router.supported-keyschemes
     Vendor-Identification       | router.vendor-identification
     ----------------------------+------------------------------------
    '''

    __legacyToNew = {}
    __newToLegacy = {}

    def __init__(self):
        self.addLegacy("router.attribute.max-count", "Attribute.Max-Count")
        self.addLegacy("router.attribute.name.max-length", "Attribute.Name.Max-Length")
        self.addLegacy("router.attribute.opaque.max-length", "Attribute.Opaque.Max-Length")
        self.addLegacy("router.attribute.string.max-length", "Attribute.String.Max-Length")
        self.addLegacy("router.packet.max-length", "Packet.Max-Length");
        self.addLegacy("router.recv-queue.drop-policy", "Receive-Queue.Drop-Policy")
        self.addLegacy("router.recv-queue.max-length", "Receive-Queue.Max-Length")
        self.addLegacy("router.send-queue.drop-policy", "Send-Queue.Drop-Policy")
        self.addLegacy("router.send-queue.max-length", "Send-Queue.Max-Length")
        self.addLegacy("router.subscription.max-count", "Subscription.Max-Count")
        self.addLegacy("router.subscription.max-length", "Subscription.Max-Length")
        self.addLegacy("router.supported-keyschemes", "Supported-Key-Schemes")
        self.addLegacy("router.vendor-identification", "Vendor-Identification")
        self.addLegacy("router.coalesce-delay", "TCP.Send-Immediately")

    def addLegacy(self, oldOption, newOption):
        self.__legacyToNew[oldOption] = newOption
        self.__newToLegacy[newOption] = oldOption

    def legacyToNew(self, option):
        o = self.__legacyToNew[option]
        if (o):
            return o
        else:
            return option

    def newToLegacy(self, option):
        o = self.__newToLegacy[option]
        if (o):
            return o
        else:
            return option

    def setWithLegacy(self, options, option, value):
        '''
        Sets the given value for the legacy and its successor option in options 
        '''
        options[option] = value
        options[self.__newToLegacy(option)] = value
