import abc
import io
import itertools

from avis import xdrcoding
from avis.avishelpers import BinaryStream
from avis.exceptions import ProtocolCodecError
from avis.security.keysets import Keys


class Message(object):
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def typeId(self):
        '''Return the ID of this message type'''
        pass

    @abc.abstractmethod
    def encode(self, stream):
        '''Encode the message to the given stream'''
        pass

    @abc.abstractmethod
    def decode(self, stream):
        '''Decode the message from the given stream'''
        pass

    def name(self):
        '''
        Type name of this message
        '''
        return str(type(self))


class XidMessage(Message):
    __metaclass__ = abc.ABCMeta
    '''
    Base class for messages that use a transaction id to identify replies.
    '''
    __staticXidCounter = itertools.count(1)

    xid = -1

    def __init__(self, xid=-1):
        self.xid = xid;

    def hasValidXid(self):
        return self.xid > 0

    def encode(self, stream):
        bs = BinaryStream(stream)
        bs.writeInt32(self.xid)

    def decode(self, stream):
        bs = BinaryStream(stream)
        self.xid = bs.readInt32()

        if (self.xid <= 0):
            raise ProtocolCodecError("XID must be >= 0 (was" + self.xid + ")")

    @classmethod
    def nextXid(cls):
        # NOTE: XID must not be zero (sec 7.4)        
        return XidMessage.__staticXidCounter.__next__()


class RequestMessage(XidMessage):
    __metaclass__ = abc.ABCMeta
    '''
    A XID-based request message that has a defined reply message.
    '''

    def __init__(self, xid=-1):
        super().__init__(xid)

    @abc.abstractmethod
    def replyType(self):
        '''
        The type of a successful reply.
        '''
        pass


class SyntheticMessage(Message):
    '''
    Synthetic message used internally
    '''

    def encode(self, stream):
        raise ProtocolCodecError("Synthetic message")

    def decode(self, stream):
        raise ProtocolCodecError("Synthetic message")


# Keep alive message
class ConfConn(Message):
    ID = 64

    def typeId(self):
        return ConfConn.ID

    def encode(self, stream):
        pass

    def decode(self, stream):
        pass


# ConnectionRequest
class ConnRqst(RequestMessage):
    ID = 49

    versionMajor = 0
    versionMinor = 0
    options = {}

    notificationKeys = Keys()
    subscriptionKeys = Keys()

    def __init__(self, major, minor):
        super().__init__(XidMessage.nextXid())
        self.versionMajor = major
        self.versionMinor = minor

    def typeId(self):
        return ConnRqst.ID

    def replyType(self):
        return type(ConnRply)

    def encode(self, stream):
        super().encode(stream)
        bs = BinaryStream(stream)

        bs.writeInt32(self.versionMajor)
        bs.writeInt32(self.versionMinor)

        xdrcoding.putNameValues(stream, self.options);

        self.notificationKeys.encode(stream)
        self.subscriptionKeys.encode(stream)

    def decode(self, stream):
        super().decode(stream)
        bs = BinaryStream(stream)

        self.versionMajor = bs.readInt32()
        self.versionMinor = bs.readInt32()

        options = xdrcoding.getNameValues(stream);

        self.notificationKeys = Keys.decode(stream)
        self.subscriptionKeys = Keys.decode(stream)


# Connection reply
class ConnRply(XidMessage):
    ID = 50

    options = {}

    def typeId(self):
        return ConnRply.ID

    def encode(self, stream):
        super().encode(stream)
        xdrcoding.putNameValues(stream, self.options);

    def decode(self, stream):
        super().decode(stream)
        options = xdrcoding.getNameValues(stream);


# Disconnect message
class Disconn(Message):
    ID = 53

    (REASON_UNKNOWN, REASON_SHUTDOWN, REASON_SHUTDOWN_REDIRECT, REASON_PROTOCOL_VIOLATION) = range(4)

    reason = REASON_UNKNOWN
    args = ""

    def typeId(self):
        return Disconn.ID

    def hasArgs(self):
        return len(self.args) > 0

    def encode(self, stream):
        if (self.reason == Disconn.REASON_UNKNOWN):
            raise ProtocolCodecError("Reason not set.")

        bs = BinaryStream(stream)
        bs.writeInt32(self.reason)
        xdrcoding.putString(stream, self.args)

    def decode(self, stream):
        bs = BinaryStream(stream)
        self.reason = bs.readInt32()
        args = xdrcoding.getString(stream)


# Disconnect reply
class DisconnRply(XidMessage):
    ID = 52

    def typeId(self):
        return DisconnRply.ID


# Disconnect request
class DisconnRqst(RequestMessage):
    ID = 51

    def __init__(self):
        super().__init__(XidMessage.nextXid())

    def typeId(self):
        return DisconnRqst.ID

    def replyType(self):
        return type(DisconnRply)


# Warning sent by the server when packets may have been dropped
class DropWarn(Message):
    ID = 62

    def typeId(self):
        return DropWarn.ID

    def encode(self, stream):
        pass

    def decode(self, stream):
        pass


class ErrorMessage(SyntheticMessage):
    '''
    Synthetic error message
    '''
    ID = -1

    error = None
    # message which caused the error
    cause = None

    def typeId(self):
        return ErrorMessage.ID

    def __init__(self, error, cause):
        self.error = error
        self.cause = cause

    def formattedMessage(self):
        s = ""
        if self.cause == None:
            s += "Error decoding XDR frame"
        else:
            s += "Error decoding " + self.cause.name

        if self.error != None:
            s += ": " + self.error.value

        return s


class LivenessFailureMessage(SyntheticMessage):
    ID = -3

    def typeId(self):
        return LivenessFailureMessage.ID


class Nack(XidMessage):
    ID = 48

    NACK_ERR_PROT_INCOMPAT = 1
    NACK_ERR_PROT_ERROR = 1001
    NACK_ERR_NO_SUCH_SUB = 1002
    NACK_ERR_IMPL_LIMIT = 2006
    NACK_ERR_NOT_IMPL = 2007
    NACK_ERR_PARSE_ERROR = 2101
    NACK_ERR_EXP_IS_TRIVIAL = 2110

    error = None
    message = ""
    args = []

    def typeId(self):
        return Nack.ID

    def errorCodeText(self):
        '''
        Return the error text for the NACK error code.
        '''
        return self.__errorTextFor(self.error)

    def formattedMessage(self):
        '''
        Generate a formatted message from the message template returned
        by the router. e.g. expand the %1 and %2 in "%1: Expression '%2'
        does not refer to a name" to the values in <tt>arg [0]</tt> and
        <tt>arg [1]</tt>.
        '''
        if len(self.args) == 0:
            return self.message
        else:
            s = self.message
            for i in range(0, len(self.args)):
                s = self.__replace(s, i + 1, self.args[i])
            return s

    def __replace(self, s, argNumber, arg):
        tag = "%" + str(argNumber)
        return s.replace(tag, arg)

    def __errorTextFor(self, error):
        if error == Nack.NACK_ERR_PROT_INCOMPAT:
            return "Incompatible protocol"
        elif error == Nack.NACK_ERR_PROT_ERROR:
            return "Communication protocol error"
        elif error == Nack.NACK_ERR_NO_SUCH_SUB:
            return "Unknown subscription ID"
        elif error == Nack.NACK_ERR_IMPL_LIMIT:
            return "Exceeded client connection resource limit"
        elif error == Nack.NACK_ERR_NOT_IMPL:
            return "Feature not implemented"
        elif error == Nack.NACK_ERR_PARSE_ERROR:
            return "Subscription parse error"
        elif error == Nack.NACK_ERR_EXP_IS_TRIVIAL:
            return "Expression is trivial (constant)"
        else:
            return "Error code " + str(error)

    def encode(self, stream):
        super().encode(stream)
        bs = BinaryStream(stream)
        bs.writeInt32(self.error)
        xdrcoding.putString(stream, self.message)
        xdrcoding.putObjects(stream, self.args)

    def decode(self, stream):
        super().decode(stream)
        bs = BinaryStream(stream)
        self.error = bs.readInt32()
        self.message = xdrcoding.getString(stream)
        self.args = xdrcoding.getObjects(stream)


class Notify(Message):
    __metaclass__ = abc.ABCMeta
    '''
    Base class for notify messages.
    '''
    attributes = {}
    deliverInsecure = True
    keys = Keys()

    def encode(self, stream):
        xdrcoding.putNameValues(stream, self.attributes)
        xdrcoding.putBool(stream, self.deliverInsecure)
        self.keys.encode(stream)

    def decode(self, stream):
        self.attributes = xdrcoding.getNameValues(stream)
        self.deliverInsecure = xdrcoding.getBool(stream)
        self.keys = Keys.decode(stream)


class NotifyEmit(Notify):
    ID = 56

    def typeId(self):
        return NotifyEmit.ID


class NotifyDeliver(Message):
    ID = 57

    attributes = {}
    secureMatches = []  # array of longs
    insecureMatches = []  # array of longs

    def typeId(self):
        return NotifyDeliver.ID

    def encode(self, stream):
        xdrcoding.putNameValues(stream, self.attributes)
        xdrcoding.putLongArray(stream, self.secureMatches)
        xdrcoding.putLongArray(stream, self.insecureMatches)

    def decode(self, stream):
        self.attributes = xdrcoding.getNameValues(stream)
        self.secureMatches = xdrcoding.getLongArray(stream)
        self.insecureMatches = xdrcoding.getLongArray(stream)


class QuenchPlaceHolder(XidMessage):
    '''
    Placeholder for QnchAddRqst, QnchModRqst and QnchDelRqst that
    allows them to be decoded and sent to server. Server will currently NACK.
    '''
    ID = -2

    ADD = 80
    MODIFY = 81
    DELETE = 82

    messageType = -1
    length = 0

    def typeId(self):
        return QuenchPlaceHolder.ID

    def encode(self, stream):
        raise NotImplementedError("This is just a quench placeholder for now")

    def decode(self, stream):
        super().decode(stream)
        stream.seek(self.length - 4, io.SEEK_CUR)


class RequestTimeoutMessage(SyntheticMessage):
    '''
    Synthetic message generated when a request timeout has elapsed.
    '''
    ID = -2

    # The request that timed out.
    request = None

    def typeId(self):
        return RequestTimeoutMessage.ID


class SecRqst(RequestMessage):
    ID = 54

    addNtfnKeys = Keys()
    delNtfnKeys = Keys()
    addSubKeys = Keys()
    delSubKeys = Keys()

    def __init(self):
        super().__init__(XidMessage.nextXid())

    def typeId(self):
        return SecRqst.ID

    def replyType(self):
        return type(SecRply)

    def encode(self, stream):
        super().encode(stream)

        self.addNtfnKeys.encode(stream)
        self.delNtfnKeys.encode(stream)
        self.addSubKeys.encode(stream)
        self.delSubKeys.encode(stream)

    def decode(self, stream):
        super().decode(stream)

        self.addNtfnKeys = Keys.decode(stream)
        self.delNtfnKeys = Keys.decode(stream)
        self.addSubKeys = Keys.decode(stream)
        self.delSubKeys = Keys.decode(stream)


class SecRply(XidMessage):
    ID = 55

    def typeId(self):
        return SecRply.ID


class SubAddRqst(RequestMessage):
    ID = 58

    subscriptionExpr = ""
    acceptInsecure = True
    keys = Keys()

    def __init__(self):
        super().__init__(XidMessage.nextXid())

    def typeId(self):
        return SubAddRqst.ID

    def replyType(self):
        return type(SubRply)

    def encode(self, stream):
        super().encode(stream)

        xdrcoding.putString(stream, self.subscriptionExpr)
        xdrcoding.putBool(stream, self.acceptInsecure)
        self.keys.encode(stream)

    def decode(self, stream):
        super().decode(stream)

        self.subscriptionExpr = xdrcoding.getString(stream)
        self.acceptInsecure = xdrcoding.getBool(stream)
        self.keys = Keys.decode(stream)


class SubDelRqst(RequestMessage):
    ID = 60
    subscriptionId = -1

    def __init__(self):
        super().__init__(XidMessage.nextXid())

    def typeId(self):
        return SubDelRqst.ID

    def replyType(self):
        return type(SubRply)

    def encode(self, stream):
        super().encode(stream)

        bs = BinaryStream(stream)
        bs.writeInt64(self.subscriptionId)

    def decode(self, stream):
        super().decode(stream)

        bs = BinaryStream(stream)
        self.subscriptionId = bs.readInt64()


class SubModRqst(RequestMessage):
    ID = 59

    subscriptionId = -1
    subscriptionExpr = ""
    acceptInsecure = True
    addKeys = Keys()
    delKeys = Keys()

    def __init__(self):
        super().__init__(XidMessage.nextXid())

    def typeId(self):
        return SubModRqst.ID

    def replyType(self):
        return type(SubRply)

    def encode(self, stream):
        super().encode(stream)
        bs = BinaryStream(stream)

        bs.writeInt64(self.subscriptionId)
        xdrcoding.putString(stream, self.subscriptionExpr)
        xdrcoding.putBool(stream, self.acceptInsecure)
        self.addKeys.encode(stream)
        self.delKeys.encode(stream)

    def decode(self, stream):
        super().decode(stream)
        bs = BinaryStream(stream)

        self.subscriptionId = bs.readInt64()
        self.subscriptionExpr = xdrcoding.getString(stream)
        self.acceptInsecure = xdrcoding.getBool(stream)
        self.addKeys = Keys.decode(stream)
        self.delKeys = Keys.decode(stream)


class SubRply(XidMessage):
    ID = 61

    subscriptionId = -1

    def typeId(self):
        return SubRply.ID

    def encode(self, stream):
        super().encode(stream)
        bs = BinaryStream(stream)

        bs.writeInt64(self.subscriptionId)

    def decode(self, stream):
        super().decode(stream)
        bs = BinaryStream(stream)

        self.subscriptionId = bs.readInt64()


class TestConn(Message):
    ID = 63

    def typeId(self):
        return TestConn.ID

    def encode(self, stream):
        pass

    def decode(self, stream):
        pass


class UNotify(Notify):
    ID = 32

    clientMajorVersion = 0
    clientMinorVersion = 0

    def typeId(self):
        return UNotify.ID

    def encode(self, stream):
        bs = BinaryStream(stream)
        bs.writeInt32(self.clientMajorVersion)
        bs.writeInt32(self.clientMinorVersion)

        super().encode(stream)

    def decode(self, stream):
        bs = BinaryStream(stream)

        self.clientMajorVersion = bs.readInt32()
        self.clientMinorVersion = bs.readInt32()

        super().decode(stream)
