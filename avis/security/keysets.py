'''
Created on 26.02.2013

@author: berre
'''

import abc

from avis.avishelpers import BinaryStream


class KeySet(abc.ABCMeta):
    '''
    A polymorphic key set stored as part of a {@link Keys} key
    collection: may be either a single set of Key items or a dual set
    for the dual key schemes. Clients should not generally need to
    access key sets directly: use the {@link Keys} class instead.
    '''

    @abc.abstractmethod
    def count(self):
        pass

    @abc.abstractmethod
    def isEmpty(self):
        pass

    @abc.abstractmethod
    def add(self, key):
        pass

    @abc.abstractmethod
    def remove(self, key):
        pass

    @abc.abstractmethod
    def addKeys(self, keys):
        pass

    @abc.abstractmethod
    def removeKeys(self, keys):
        pass

    @abc.abstractmethod
    def subtract(self, keys):
        '''
        Return this keyset with the given set removed.
        '''
        pass


# dummy
class SingleKeySet(KeySet):
    '''
    A single set of keys.
    '''

    keys = []


class Key:
    '''
    A key value used to secure notifications. A key is simply an
    immutable block of bytes.

    Elvin defines two types of key, <em>private</em> (or <em>raw</em>)
    keys, and <em>public</em> (or <em>prime</em>) keys. A public
    key is a one-way hash (e.g. using SHA-1) of a private key. A
    private key may be any random data, or simply a password. A private
    key is defined to match a public key if the corresponding hash of
    its data matches the public key's data, e.g. if
    <code>sha1 (privateKey.data) == publicKey.data</code>.
    
    Note that this is not a public key system in the RSA sense but
    that, like RSA, public keys can be shared in the open without loss
    of security.
    
    This class precomputes a hash code for the key data to accelerate
    equals () and hashCode ().
    
    A key value used to secure notifications. A key is simply an
    immutable block of bytes.
    '''

    data = bytes()
    __hashCode = 0

    def __init__(self, password):
        self.data = bytes(password, "utf-8")
        self.__hashCode = hash(self.data)

    def __hash__(self):
        return self.__hashCode

    def __eq__(self, other):
        return self.data == other.data

    def publicKeyFor(self, scheme):
        '''
        Shortcut to generate the public (prime) key for a given scheme.
        '''
        return scheme.publicKeyFor(self)


# so far only a dummy
class Keys:
    '''         
    A key collection used to secure notifications. A key collection
    contains zero or more mappings from a key scheme to the keys registered for that scheme.
    Once in use, key collections should be treated as immutable
    i.e. never be modified directly after construction.
    (See also section 7.4 of the client protocol spec.)
    '''

    def encode(self, stream):
        bs = BinaryStream(stream)

        bs.writeInt32(0)
        # foreach entry in keySets
        # ...

    @classmethod
    def decode(cls, stream):
        bs = BinaryStream(stream)

        k = Keys()

        length = bs.readInt32()

        if (length > 0):
            # Todo
            pass

        return k
