# Common Avis definitions

DEFAULT_PORT = 2917

CLIENT_VERSION_MAJOR = 4
CLIENT_VERSION_MINOR = 0

DEFAULT_PROTOCOL = ["tcp", "none", "xdr"]

SECURE_PROTOCOL = ["ssl", "none", "xdr"]

# Require secure key match between notification and subscription
# before delivering to a client.        
SECURE_MODE_REQUIRE_SECURE_DELIVERY = 0

# Allow clients without matching keys to receive the message. Those
# with matching keys will still receive securely.
SECURE_MODE_ALLOW_INSECURE_DELIVERY = 1
