'''

Class used to connect to and communicate with an Elvin server

Created on 04.03.2013

@author: berre
'''

from threading import Event
from threading import Lock

from avis import common
from avis.client.notification import Notification
from avis.client.subscription import Subscription
from avis.connectors import ElvinConnector
from avis.exceptions import NotConnectedError, ConnectionOptionsError
from avis.msg import *
from avis.options import ElvinOptions, ConnectionOptions
from avis.utils.events import EventHook


class Elvin:
    '''
    The core class in the Avis client library which manages a client's
    connection to an Elvin router. Typically a client creates a
    connection and then subscribes to notifications and/or
    sends them.
    '''

    # maps subscription id <-> subscription instance
    subscriptions = {}

    # the elvin connector used to connect to the elvin router
    connector = None

    # the connection liveness monitor
    livenessMonitor = None

    # the routers ElvinURI
    routerUri = None

    # The current options for the connection. These options
    # reflect any changes made after initialization, e.g. by using
    # {@link #setReceiveTimeout(long)}, {@link
    # setNotificationKeys(Keys)}, etc.
    options = None

    # The connection options established with the router. Changing
    # these after connecting to a server yields no effect
    connectionOptions = None

    # used to aquire locks for critical sections
    __lock = Lock()

    # A method which handles received messages with higher priority before
    # forwarding the messages to outside handlers
    # (may suppress the forwarding)
    messagePriorityHandler = None

    # event which is fired when a notification has been received
    # parameters: subscription, notification, secure
    onNotificationReceived = EventHook()

    # Close Reasons

    # The client was shut down normally with a call to Elvin.close()
    CLOSE_REASON_CLIENT_SHUTDOWN = 0
    # The router was shut down normally
    CLOSE_REASON_ROUTER_SHUTDOWN = 1
    # The router failed to respond to a liveness check. Either the
    # router has crashed, or network problems have stopped messages
    # getting through.
    CLOSE_REASON_ROUTER_STOPPED_RESPONDING = 2
    # The network connection to the router was terminated abnormally
    # without the standard shutdown protocol. Most likely the network
    # connection between client and router has been disconnected.
    CLOSE_REASON_ROUTER_SHUTDOWN_UNEXPECTEDLY = 3
    # Either the client or the router decided that the protocol rules
    # have been violated. This would only happen in the case of a
    # serious bug in the client or router or version incompatibilities
    CLOSE_REASON_PROTOCOL_VIOLATION = 4
    # An I/O exception was thrown while communicating with the router.
    # The exception will be in the error field.
    CLOSE_REASON_IO_ERROR = 5

    # Events

    # Event which is fired when the connection is closed
    # parameters are: reason, message, error
    onClosed = EventHook()

    def isConnected(self):
        return self.connector != None and self.connector.isConnected()

    __receiveTimeout = 10000  # ten seconds

    def getReceiveTimeout(self):
        return self.__receiveTimeout

    def setReceiveTimeout(self, value):
        if value < 0:
            raise TypeError("ReceiveTimeout must be greater than or equal to zero")

        self.__receiveTimeout = value
        # self.livenessMonitor.setReceiveTimeout(value)

    __livenessTimeout = 60000  # 60 seconds

    def getLivenessTimeout(self):
        return self.__livenessTimeout

    def setLivenessTimeout(self, value):
        if value < 0:
            raise TypeError("LivenessTimeout must be greater than or equal to zero")

        self.__livenessTimeout = value
        # self.livenessMonitor.setLivenessTimeout(value)

    def connect(self, routerUri, options=ElvinOptions(ConnectionOptions(), Keys(), Keys())):
        '''
        Create a new connection to an Elvin router.

        routerUri: The URI of the router to connect to.
        options: The Elvin client options. Modifying these after they
                 have been passed into this constructor has no effect.
        '''
        self.routerUri = routerUri
        self.options = options

        # reset subscriptions
        self.subscriptions.clear()

        if (self.routerUri.protocol != common.DEFAULT_PROTOCOL and
                    self.routerUri.protocol != common.SECURE_PROTOCOL):
            raise TypeError(
                "Avis only supports the following protocols: " + str(common.DEFAULT_PROTOCOL) + " and " + str(
                    common.SECURE_PROTOCOL) + " : " + str(self.routerUri))

        try:
            if not self.__openConnection():
                return False

            # send connection request to router
            connRqst = ConnRqst(self.routerUri.versionMajor, self.routerUri.versionMinor)
            connRqst.options = options.connectionOptions.asMapWithLegacy()
            connRqst.notificationKeys = options.notificationKeys
            connRqst.subscriptionKeys = options.subscriptionKeys

            connRply = self.__sendAndReceive(connRqst)
            if connRply == None:
                raise TimeoutError("The connection attempt timed out.")

            acceptedOptions = ConnectionOptions().convertLegacyToNew(connRply.options)
            rejectedOptions = options.connectionOptions.differenceFrom(acceptedOptions)

            if len(rejectedOptions) > 0:
                raise ConnectionOptionsError(options.connectionOptions, rejectedOptions)

            # include any options the router has added that we didn't specify
            options.updateConnectionOptions(acceptedOptions)

            # todo: create liveness monitor
            # livenessMonitor = new LivenessMonitor(connector, LivenessTimeout, ReceiveTimeout);
            # livenessMonitor.ConnectionDied += livenessMonitor_ConnectionDied;
            # livenessMonitor.Enable();

            return True
        except Exception:
            self.close()
            return False

    def __openConnection(self):
        '''
        Open a network connection to the router.
        '''
        try:

            if (self.connector == None):
                # construct the connector object (only once)
                self.connector = ElvinConnector()

                # handle the low-level message receive
                self.connector.onMessageReceived += self.__connector_MessageReceived

                # handle connection close
                self.connector.onConnectionClosed += self.__connector_ConnectionClosed

            # try to connect
            self.connector.connect(self.routerUri.host, self.routerUri.port)
            # TODO: support SSL/TLS self.routerUri.IsSecure, self.options.serverCertificate

            return self.connector.isConnected()
        except Exception:
            # Log?
            return False

    def close(self, reason=CLOSE_REASON_CLIENT_SHUTDOWN, message="Client shutting down normally", error=None):
        '''
        Close the connection to the router. May be executed more than
        once with no effect.
        '''
        if (self.isConnected()):
            with self.__lock:
                if self.connector != None:
                    if self.connector.isConnected():
                        if reason == Elvin.CLOSE_REASON_CLIENT_SHUTDOWN:
                            try:
                                self.sendAndReceive(DisconnRqst())
                            except Exception as ex:
                                # TODO: log
                                pass

                        self.connector.close()
                    self.connector = None

                self.onClosed.fire(reason, message, error)

    # ''''''''''''''
    # Send & Receive
    # ''''''''''''''

    def __sendAndReceive(self, request):
        '''
        Not thread-safe at the moment!
        '''

        if not isinstance(request, RequestMessage):
            raise TypeError("Only messages of the type RequestMessage are supported by sendAndReceive.")

        evt = Event()  # thread blocking event
        handleParam = {"xid": request.xid, "event": evt}
        # register priority handler
        self.messagePriorityHandler = lambda msg: self.__priorityHandlerFunc(msg, handleParam)

        self.__send(request)
        evt.wait(self.getReceiveTimeout())
        self.messagePriorityHandler = None  # no more priorized handling

        return handleParam["reply"]

    def __priorityHandlerFunc(self, message, params):

        if isinstance(message, XidMessage):
            if message.xid == params["xid"]:
                params["reply"] = message  # fill in the response message
                params["event"].set()
                return True  # handled -> do not forward the message

        return False  # Not handled

    def __connector_ConnectionClosed(self, reason):
        if (reason == ElvinConnector.CLOSE_REASON_RESET_BY_PEER):
            self.onClosed.fire(self.CLOSE_REASON_ROUTER_SHUTDOWN_UNEXPECTEDLY, "Connection reset by peer", None)
        else:
            self.close()

    def __connector_MessageReceived(self, msg):
        # a message was received
        # is there a priority handler?
        if self.messagePriorityHandler != None:
            if self.messagePriorityHandler(msg):
                return  # message handled by priorized handler, supress it

        # handle system messages
        self.__handleMessage(msg)

    def __handleMessage(self, msg):
        if msg.typeId() == NotifyDeliver.ID:
            self.__handleNotifyDeliver(msg)
        elif msg.typeId() == Disconn.ID:
            self.__handleDisconnect(msg)
        elif msg.typeId() == DropWarn.ID:
            # TODO: replac eprint with log
            print("Router sent a dropped packet warning: a message may have been discarded due to congestion")
        elif msg.typeId() == ErrorMessage.ID:
            self.__handleErrorMessage(msg)
        else:
            self.close(Elvin.CLOSE_REASON_PROTOCOL_VIOLATION, "Received unexpected message type " + msg.name())

    def __send(self, message):
        self.connector.sendMessage(message)

    # '''''''''''''
    # Subscriptions
    # '''''''''''''

    def subscribe(self, subscriptionExpr, keys=Keys(), secureMode=common.SECURE_MODE_ALLOW_INSECURE_DELIVERY):
        '''
        Create a new subscription
        '''
        if self.isConnected():
            subscription = Subscription(self, subscriptionExpr, secureMode, keys)
            self.__subscribe(subscription)
            return subscription
        else:
            raise NotConnectedError("Not connected to event router.")

    def __subscribe(self, subscription):

        subRqst = SubAddRqst()
        subRqst.subscriptionExpr = subscription.subscriptionExpr
        subRqst.keys = subscription.keys
        subRqst.acceptInsecure = subscription.acceptInsecure()
        subscription.id = self.__sendAndReceive(subRqst).subscriptionId

        # register id
        with self.__lock:
            if self.subscriptions.get(subscription.id) != None:
                self.close(Elvin.CLOSE_REASON_PROTOCOL_VIOLATION,
                           "Router issued duplicate subscription ID: " + str(subscription.Id))

            self.subscriptions[subscription.id] = subscription

    def __unsubscribe(self, subscription):
        self.__sendAndReceive(SubDelRqst(subscription.id))

        with self.__lock:
            self.subscriptions[subscription.id] = None

    def __modifyKeys(self, subscription, newKeys):
        raise NotImplementedError()

    def __modifySubscriptionExpr(self, subscription, subscriptionExpr):
        raise NotImplementedError()

    def __modifySecureMode(self, subscription, mode):
        raise NotImplementedError()

    def hasSubscription(self, subscription):
        '''
        Check if a given subscription is part of this connection.
        '''
        with self.__lock:
            return subscription in self.subscriptions.values()

    def getSubscriptions(self, ids):
        '''
        Return the subscriptions with the given ids
        '''
        lst = []
        for id in ids:
            s = self.subscriptions[id]
            if s != None:
                lst.append(s)

                # raise exception for unknown ids?

        return lst

    # '''''''''''''
    # Notifications
    # '''''''''''''

    def sendNotification(self, notification, keys=Keys(), secureMode=common.SECURE_MODE_ALLOW_INSECURE_DELIVERY):
        '''
        Send a notification
        '''
        if keys == None: raise TypeError("The parameter keys may not be null.")

        msg = NotifyEmit()
        msg.attributes = notification.attributes
        msg.deliverInsecure = secureMode == common.SECURE_MODE_ALLOW_INSECURE_DELIVERY
        msg.keys = keys

        self.__send(msg)

    # ''''''''''''''''
    # Message handlers
    # ''''''''''''''''

    def __handleNotifyDeliver(self, message):
        ntfy = Notification()
        ntfy.attributes = message.attributes

        # raise the event handlers separated by secure/insecure matches

        for sb in self.getSubscriptions(message.secureMatches):
            self.onNotificationReceived.fire(sb, ntfy, True)

        for sb in self.getSubscriptions(message.insecureMatches):
            self.onNotificationReceived.fire(sb, ntfy, False)

    def __handleDisconnect(self, disconn):
        reason = None
        message = None

        if disconn.reason == Disconn.REASON_SHUTDOWN:
            reason = Elvin.CLOSE_REASON_ROUTER_SHUTDOWN
            message = disconn.args if disconn.hasArgs() else "Router is shutting down"
        elif disconn.reason == Disconn.REASON_SHUTDOWN_REDIRECT:
            reason = Elvin.CLOSE_REASON_ROUTER_SHUTDOWN
            message = "Router suggested redirect to " + disconn.Args
        elif disconn.reason == Disconn.REASON_PROTOCOL_VIOLATION:
            reason = Elvin.CLOSE_REASON_PROTOCOL_VIOLATION
            message = disconn.args if disconn.hasArgs() else "Protocol violation"
        else:
            reason = Elvin.CLOSE_REASON_PROTOCOL_VIOLATION
            message = "Protocol violation"

        self.close(reason, message)

    def __handleErrorMessage(self, message):
        self.close(Elvin.CLOSE_REASON_PROTOCOL_VIOLATION, "Protocol error in communication with router: " +
                   message.formattedMessage(), message.error)
