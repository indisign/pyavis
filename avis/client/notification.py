'''
Created on 05.03.2013

@author: berre
'''


class Notification:
    '''
    A notification sent via an Elvin router. A notification is a set of
    field name/value pairs. Field values may be one of the following types:
    '''

    attributes = {}

    def isEmpty(self):
        return len(self.attributes) == 0

    def __len__(self):
        return len(self.attributes)

    def __init__(self, attributes={}):
        self.attributes = attributes

    # allow direct indexing of the underlying dictionary 

    def __delitem__(self, key):
        self.attributes[key] = None

    def __getitem__(self, key):
        return self.attributes[key]

    def __setitem__(self, key, value):
        self.attributes[key] = value

    def __str__(self):
        return str(self.attributes)
