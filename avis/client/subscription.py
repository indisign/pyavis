'''
Class which manages all subscriptions established with the Elvin/Avis host

Created on 04.03.2013

@author: berre
'''

from avis import common
from avis.security.keysets import Keys


class Subscription:
    '''
    A subscription to notifications from an Elvin connection.
    '''

    elvin = None
    id = -1
    subscriptionExpr = ""
    secureMode = common.SECURE_MODE_ALLOW_INSECURE_DELIVERY
    keys = Keys()

    def isActive(self):
        '''
        Test if this subscription is still able to receive notifications.
        A subscription is inactive after a {@link #remove()} or when its
        underlying connection is closed.
        '''
        return self.elvin != None and self.elvin.isConnected() and self.id > 0

    def acceptInsecure(self):
        return self.secureMode == common.SECURE_MODE_ALLOW_INSECURE_DELIVERY

    def __init__(self, elvin, subscriptionExpr, secureMode=common.SECURE_MODE_ALLOW_INSECURE_DELIVERY, keys=Keys()):

        if keys == None:
            raise TypeError("The keys parameter may not be null");
        if secureMode == None:
            raise TypeError("The secureMode parameter may not be null");

        self.elvin = elvin
        self.subscriptionExpr = self.__checkSubscription(subscriptionExpr)
        self.secureMode = secureMode
        self.keys = keys

    def remove(self):
        '''
        Remove this subscription (unsubscribe). May be called more than
        once. Unlike the other methods on this class, this may be called
        on a subscription for a connection that has been closed without
        generating an error, since such a subscription is effectively
        removed anyway.
        '''
        if (self.id != 0):
            if self.elvin.isConnected():
                self.elvin.unsubscribe(self)

            self.id = 0

    def __checkSubscription(self, subscriptionExpr):
        if subscriptionExpr == None:
            raise TypeError("Subscription cannot be null")

        subscriptionExpr = subscriptionExpr.strip()

        if len(subscriptionExpr) == 0:
            raise TypeError("Subscription expression cannot be empty");

        return subscriptionExpr;

    @classmethod
    def escapeField(cls, field):
        '''
        Escape illegal characters in a field name for use in a
        subscription expression. This can be useful when constructing
        subscription expressions dynamically.
        '''
        raise NotImplementedError()

    @classmethod
    def escapeString(cls, s):
        '''
        Escape illegal characters in a string value for use in a
        subscription expression. This can be useful when constructing
        subscription expressions dynamically.
        '''
        raise NotImplementedError()
