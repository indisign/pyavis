'''
Created on 26.02.2013

@author: berre
'''


class UriFormatError(Exception):
    uri = ""

    def __init__(self, value, uri=""):
        self.uri = uri
        self.value = value

    def __str__(self):
        if (self.uri == ""):
            return repr(self.value)
        else:
            return repr(self.value), "(", self.uri, ")"


class ProtocolCodecError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class NotConnectedError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ConnectionOptionsError(Exception):
    options = None
    rejectedOptions = None

    def __init__(self, options, rejectedOptions):
        super().__init__(
            "Router rejected connection options: rejected options and actual values: " + str(rejectedOptions))
        self.options = options
        self.rejectedOptions = rejectedOptions
