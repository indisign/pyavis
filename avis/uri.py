import re  # regular expressions

from avis import common, exceptions


class ElvinUri:
    # the regex expression to validate uris
    URL_PATTERN = "(\\w+):([^/]+)?/([^/]+)?/([^;/][^;]*)(;.*)?"

    # the original uri string as passed into the constructor
    uriString = ""

    # The URI scheme (i.e the part before the ":"). This must be "elvin" for URI's referring to Elvin routers.
    scheme = ""
    # Major protocol version (default is common.CLIENT_VERSION_MAJOR)
    versionMajor = common.CLIENT_VERSION_MAJOR
    # Minor protocol version (default is common.CLIENT_VERSION_MINOR)
    versionMinor = common.CLIENT_VERSION_MINOR
    # The stack of protocol modules in (transport, security, marshalling)
    protocol = []
    # The host name
    host = ""
    # The port (default is avis.common.DEFAULT_PORT)
    port = common.DEFAULT_PORT
    # The URI options. e.g. elvin://host:port;option1=value1;option2=value2
    options = {}

    __hashCode = 0

    def IsSecure(self):
        return self.protocol == common.SECURE_PROTOCOL

    ### constructors ###


    def __init__(self, uriString):
        """
        Create a new instance.
        """
        self.__init()
        self.uriString = uriString
        self.__parseUri()
        self.__validate()
        self.__hashCode = self.__computeHash()

        '''
        def __init__(self, host, port):
        """
        Create a new instance from a host and
        port using defaults for others.
        """
        self.__init()
        self.uriString = "elvin://"+host+':'+port
        self.scheme = "elvin"
        self.host = host
        self.port = port
        self.__hashCode = self.__computeHash()
        '''

        ### init ###

    def __init(self):
        self.scheme = None
        self.versionMajor = common.CLIENT_VERSION_MAJOR
        self.versionMinor = common.CLIENT_VERSION_MINOR
        self.protocol = common.DEFAULT_PROTOCOL
        self.host = None
        self.port = common.DEFAULT_PORT
        self.options = {}

    def init(self, defaultUri):
        """
        Copy defaultUri's values to this uri
        """
        self.uriString = defaultUri.uriString
        self.scheme = defaultUri.scheme
        self.versionMajor = defaultUri.versionMajor
        self.versionMinor = defaultUri.versionMinor
        self.protocol = defaultUri.protocol
        self.host = defaultUri.host
        self.port = defaultUri.port
        self.options = defaultUri.options
        self.__hashCode = defaultUri.__hashCode

    def __validate(self):
        """
        Checks the validity of the current uri
        May throw UriFormatError
        """
        if (not self.__validScheme(self.scheme)):
            raise exceptions.UriFormatError("Invalid scheme: " + self.scheme)

    def __validScheme(self, schemeToCheck):
        """
        Check if scheme is valid. May be extended.
        """
        return schemeToCheck == "elvin"

    def __str__(self):
        return self.uriString

    def toCanonicalString(self):
        '''
        Return a canonical representation of this Uri
        (all information(scheme,version,protocol,host,port) and alphabetically ordered options
        '''
        s = "{0}:{1}.{2}/{3}/{4}:{5}".format(self.scheme, self.versionMajor, self.versionMinor,
                                             ",".join(self.protocol), self.host, self.port)

        for key in sorted(self.options.keys()):
            s += ";" + key + "=" + self.options[key]

        return s

    def __hash__(self):
        return self.__hashCode

    def __eq__(self, other):
        return self.__hashCode == other.__hashCode and \
               self.scheme == other.scheme and \
               self.host == other.host and \
               self.port == other.port and \
               self.versionMajor == other.versionMajor and \
               self.versionMinor == other.versionMinor and \
               self.options == other.options and \
               self.protocol == other.protocol

    def __computeHash(self):
        pHash = hash(self.scheme) ^ hash(self.host) ^ self.port
        for p in self.protocol:
            pHash ^= hash(p)
        return pHash

    def __parseUri(self):

        m = re.match(self.URL_PATTERN, self.uriString)

        if (not m):
            raise exceptions.UriFormatError("Not a valid Elvin URI", self.uriString)

        self.scheme = m.group(1)
        # version
        if (m.group(2)):
            self.__parseVersion(m.group(2))
        # protocol
        if (m.group(3)):
            self.__parseProtocol(m.group(3))
        # endpoint (host/port)
        self.__parseEndpoint(m.group(4))

        if (m.group(5)):
            self.__parseOptions(m.group(5))

    def __parseVersion(self, versionExpr):
        versionRegEx = "^(\\d+)(?:\\.(\\d+))?$"
        m = re.match(versionRegEx, versionExpr)
        if (m):
            try:
                self.versionMajor = int(m.groups(1)[0])
                if (m.group(2)):
                    self.versionMinor = int(m.group(2)[0])
            except:
                raise exceptions.UriFormatError("Number too large in version string: \"" + versionExpr + "\"",
                                                self.uriString)
        else:
            raise exceptions.UriFormatError("Invalid version string: \"" + versionExpr + "\"", self.uriString)

    def __parseProtocol(self, protocolExpr):
        protocolRegEx = "^(?:(\\w+),(\\w+),(\\w+))$|^secure$"
        m = re.match(protocolRegEx, protocolExpr)
        if (m):
            if (m.group(1)):
                self.protocol = protocolExpr.split(",")
            else:
                self.protocol = common.SECURE_PROTOCOL
        else:
            raise exceptions.UriFormatError("Invalid protocol: \"" + protocolExpr + "\"", self.uriString)

    def __parseEndpoint(self, endpoint):
        # choose between IPv6 and IPv4 address scheme
        if (endpoint[0] == "["):
            pattern = "^(\\[[^\\]]+\\])(?::(\\d+))?$"
        else:
            pattern = "^([^:]+)(?::(\\d+))?$"

        m = re.match(pattern, endpoint)

        if (m):
            self.host = m.group(1)
            if (m.group(2)):
                self.port = int(m.group(2))
        else:
            raise exceptions.UriFormatError("Invalid port number", self.uriString)

    def __parseOptions(self, optionsExpr):
        optionRegEx = ";([^=;]+)=([^=;]*)"

        optionsExpr = optionsExpr.rstrip()

        self.options = {}

        # we cannot use re.finditer(...) because we need to make
        # sure that the whole option string matches
        m = re.match(optionRegEx, optionsExpr)
        while m:
            self.options[m.group(1)] = m.group(2)

            # remove the match from the option string
            optionsExpr = re.sub(optionRegEx, "", optionsExpr, 1)
            m = re.match(optionRegEx, optionsExpr)

        # did all parts of the string match?
        # if not the given option set is invalid
        if (optionsExpr != ""):
            raise exceptions.UriFormatError("Invalid options: \"" + optionsExpr + "\"", self.uriString)
