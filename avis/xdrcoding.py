'''
Encoding/decoding helpers for the Elvin XDR wire format.

Created on 27.02.2013

@author: berre
'''

import io

from avis.avishelpers import BinaryStream
from avis.exceptions import ProtocolCodecError

# Type codes from client protocol spec.
TYPE_INT32 = 1
TYPE_INT64 = 2
TYPE_REAL64 = 3
TYPE_STRING = 4
TYPE_OPAQUE = 5

# needed to decide whether anumber should be stored as int32 or int64
MIN_INT32 = - 0x80000000
MAX_INT32 = 0x7FFFFFFF

EMPTY_BYTES = bytes()


def getPositiveInt(stream):
    '''
    Read an int >= 0 or generate an exception.
    '''
    bs = BinaryStream(stream)
    v = bs.readInt32()
    if (v >= 0):
        return v
    else:
        raise ProtocolCodecError("Length cannot be negative (was " + v + ")")


def paddingFor(length):
    '''
    Calculate the padding needed for the size of a block of bytes to
    be a multiple of 4.
    '''
    return (4 - (length & 3)) & 3;  # this is equivalent to (4 - length % 4) % 4


def putPadding(stream, length):
    b = bytes([0])  # single null byte
    for i in range(0, paddingFor(length)):
        stream.write(b)


def getString(stream):
    '''
    Read a length-delimited 4-byte-aligned UTF-8 string.
    '''
    length = getPositiveInt(stream)
    if (length == 0):
        return ""

    bs = BinaryStream(stream)
    b = bs.readBytes(length)
    s = str(b, "utf-8")

    stream.seek(paddingFor(length), io.SEEK_CUR)
    return s


def putString(stream, s):
    '''
    Write a length-delimited 4-byte-aligned UTF-8 string.
    '''
    bs = BinaryStream(stream)

    if len(s) == 0:
        bs.writeInt32(0)
    else:
        b = bytes(s, "utf-8")
        bs.writeInt32(len(b))
        bs.writeBytes(b)

        putPadding(stream, len(b))


def putNameValues(stream, nameValues):
    '''
    Write a name/value set.
    '''
    bs = BinaryStream(stream)
    bs.writeInt32(len(nameValues))
    for key in nameValues.keys():
        putString(stream, key)
        putObject(stream, nameValues[key])


def getNameValues(stream):
    '''
    Read a name/value set and return it as dictionary
    '''
    pairs = getPositiveInt(stream)
    if (pairs == 0):
        return {}

    nameValues = {}
    for i in range(0, pairs):
        s = getString(stream)
        v = getObject(stream)
        nameValues[s] = v
    return nameValues


def putObjects(stream, objects):
    bs = BinaryStream(stream)
    bs.writeInt32(len(objects))

    for o in objects:
        putObject(stream, o)


def getObjects(stream):
    objects = []
    cnt = getPositiveInt(stream)
    for i in range(0, cnt):
        objects.append(getObject(stream))
    return objects


def putObject(stream, value):
    '''
    Put an object value in type_id/value format.
    Since there is no distinction between int and long in python,
    ints are stored as long as they are in the range [-214783648, 214783647] otherwise
    they are stored as long 
    if you want to store longs explicitly call putLong 
    '''
    bs = BinaryStream(stream)

    if value == None:
        raise ValueError("Value cannot be null")
    elif isinstance(value, str):
        bs.writeInt32(TYPE_STRING)
        putString(stream, value)
    elif isinstance(value, int):
        if (value >= MIN_INT32 and value <= MAX_INT32):
            # store as int32
            bs.writeInt32(TYPE_INT32)
            bs.writeInt32(value)
        else:
            # too large for int32 store as int64
            bs.writeInt32(TYPE_INT64)
            bs.writeInt64(value)
    elif isinstance(value, float):
        bs.writeInt32(TYPE_REAL64)
        bs.writeDouble(value)
    elif isinstance(value, bytearray):
        bs.writeInt32(TYPE_OPAQUE)
        putBytes(stream, value)
    else:
        raise ValueError("Don't know how to encode " + type(value))


def getObject(stream):
    '''
    Read an object in type_id/value format.
    '''
    bs = BinaryStream(stream)
    typeCode = bs.readInt32()

    if typeCode == TYPE_INT32:
        return bs.readInt32()
    elif typeCode == TYPE_INT64:
        return bs.readInt64()
    elif typeCode == TYPE_REAL64:
        return bs.readDouble()
    elif typeCode == TYPE_STRING:
        return getString(stream)
    elif typeCode == TYPE_OPAQUE:
        return getBytes(stream)
    else:
        raise ProtocolCodecError("Unknown type code: " + str(typeCode))


def putBytes(stream, byteData):
    '''
    Write a length-delimited, 4-byte-aligned byte array.
    '''
    bs = BinaryStream(stream)
    bs.writeInt32(len(byteData))
    bs.writeBytes(byteData)

    putPadding(stream, len(byteData))


def getBytes(stream, length=-1):
    '''
    Read a length-delimited, 4-byte-aligned byte array.
    If the length parameter is omitted or -1 the length
    is read from the stream itself
    '''
    if length == -1:
        return getBytes(stream, getPositiveInt(stream))
    else:
        bs = BinaryStream(stream)
        b = bs.readBytes(length)
        stream.seek(paddingFor(length), io.SEEK_CUR)
        return b


def putBool(stream, value):
    bs = BinaryStream(stream)
    if value:
        bs.writeInt32(1)
    else:
        bs.writeInt32(0)


def getBool(stream):
    bs = BinaryStream(stream)
    v = bs.readInt32()
    if v == 0:
        return False
    elif v == 1:
        return True
    else:
        raise ProtocolCodecError("Cannot interpret " + v + " as boolean")


def getLongArray(stream):
    '''
    Read a length-delimited array of longs.
    '''
    cnt = getPositiveInt(stream)
    longs = []
    bs = BinaryStream(stream)
    for i in range(0, cnt):
        longs.append(bs.readInt64())
    return longs


def putLongArray(stream, longs):
    '''
    Write a length-delimited array of longs.
    '''
    bs = BinaryStream(stream)
    bs.writeInt32(len(longs))
    for l in longs:
        bs.writeInt64(l)


def getStringArray(stream):
    '''
    Read a length-delimited array of strings.
    '''
    cnt = getPositiveInt(stream)
    strings = []
    bs = BinaryStream(stream)
    for i in range(0, cnt):
        strings.append(getString(stream))
    return strings


def putStringArray(stream, strings):
    '''
    Write a length-delimited array of strings.
    '''
    bs = BinaryStream(stream)
    bs.writeInt32(len(strings))
    for s in strings:
        putString(stream, s)
