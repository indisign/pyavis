'''
Socket connection handler to connect with an Elvin/Avis server

Created on 04.03.2013

@author: berre
'''

import errno
import io
import socket
import threading
import time
from threading import Event

import Queue as queue

from avis import protocol
from avis.avishelpers import BinaryStream
from avis.utils.events import EventHook


class ElvinConnector:
    '''
    TCP Client to connect with an Elvin/Avis server

    The open connection is held within a separate thread
    '''
    RCV_BUF_LEN = 1024

    # Connection timeout in seconds
    CONNECTION_TIMEOUT = 5

    # Values given to onConnectionClosed event
    CLOSE_REASON_USER = 1  # connection closed as requested by user
    CLOSE_REASON_RESET_BY_PEER = 2  # the connection has been reset by the peer

    hostname = ""
    port = -1

    msgBuf = None
    isReceiving = False
    bytesToReceive = 0

    __cancelThread = False
    __usedSocket = None

    # set when the connection is lost unexpectedly
    __connectionResetByPeer = False

    sendQueue = queue.Queue()

    # event which is fired when a message has been received
    onMessageReceived = EventHook()

    # event which is fired when an existing connection is closed or lost
    # one parameter, close reason
    onConnectionClosed = EventHook()

    # event which is set, when the connection attempt has finished
    __connAttemptFinished = Event()

    # The connection handling thread
    __connThread = None

    def connect(self, hostname, port):
        self.hostname = hostname
        self.port = port

        self.__connAttemptFinished.clear()
        self.__connectionResetByPeer = False
        self.__connThread = threading.Thread(target=self.__connLopp)
        self.__connThread.start()
        # wait for the connection
        self.__connAttemptFinished.wait(ElvinConnector.CONNECTION_TIMEOUT)

    def isConnected(self):
        return self.__usedSocket != None

    def __connLopp(self):
        '''
        The send/receive loop
        '''

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__usedSocket = s
        # set the timeout
        self.__usedSocket.settimeout(ElvinConnector.CONNECTION_TIMEOUT)
        try:
            s.connect((self.hostname, self.port))
        except:
            # connection error
            # TODO: log?
            self.__usedSocket = None
            return
        finally:
            self.__connAttemptFinished.set()

        # use non-blocking i/o
        s.setblocking(0)

        while not self.__cancelThread:

            # We have to delay here since we use non-blocking io and
            # the GIL _will_ effectively kill the performance of the GUI otherwise (tested!)
            # This also means that packets will be sent/received with a maximum 1 second
            # delay which seems acceptable for our purpose
            # see also: http://wiki.python.org/moin/GlobalInterpreterLock
            # Better would be using blocking or real async sending/receiving but then we could not
            # send/receive simultaneously easily
            time.sleep(1)

            try:
                b = s.recv(ElvinConnector.RCV_BUF_LEN)

                if (b != None and len(b) > 0):
                    # new packet?
                    if not self.isReceiving:
                        # prepare to receive new message

                        msgBuf = io.BytesIO()
                        msgBuf.write(b)

                        if msgBuf.tell() >= 4:  # at least 4 bytes have been received? (frame size)
                            self.bytesToReceive = self.__readFrameSize(msgBuf)
                        else:
                            self.bytesToReceive = -1  # we do not know how many bytes to receive yet

                        self.isReceiving = True
                    else:
                        msgBuf.write(b)

                        if (self.bytesToReceive == -1 and msgBuf.tell() >= 4):
                            self.bytesToReceive = self.__readFrameSize(msgBuf)  # get the frame size, finally

                    if self.bytesToReceive > -1 and msgBuf.tell() >= self.bytesToReceive:
                        # the whole message arrived
                        self.isReceiving = False
                        # decode the message
                        msgBuf.seek(0, io.SEEK_SET)
                        rcvdMsg = protocol.decodeMessage(msgBuf)

                        self.onMessageReceived.fire(rcvdMsg)
            except socket.error as error:
                if error.errno == errno.ECONNRESET:
                    self.__connectionResetByPeer = True
                    break  # connection lost -> leave thread loop

                pass

            # any messages to send?
            try:
                message = self.sendQueue.get_nowait()

                # encode the message
                buf = io.BytesIO()
                protocol.encodeMessage(message, buf)

                # send blocking or the receiving code gets confused (for whatever reason)
                s.setblocking(1)
                s.send(buf.getbuffer())
                s.setblocking(0)
            except queue.Empty:
                pass
            except socket.error as error:
                if error.errno == errno.ECONNRESET:
                    self.__connectionResetByPeer = True
                    break  # connection lost -> leave thread loop

                pass

        # Thread cancelled, close connection
        self.__usedSocket = None
        try:
            s.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass

        s.close()

        # fire connection closed event
        if (self.__connectionResetByPeer):
            self.onConnectionClosed.fire(self.CLOSE_REASON_RESET_BY_PEER)
        else:
            self.onConnectionClosed.fire(self.CLOSE_REASON_USER)

    def __readFrameSize(self, stream):
        oldPos = stream.tell()

        stream.seek(0, io.SEEK_SET)

        # read the frame size
        bs = BinaryStream(stream)
        frameSize = bs.readInt32()

        stream.seek(oldPos, io.SEEK_SET)

        return frameSize;

    def sendMessage(self, message):
        '''
        Send the given message to the server
        Non-blocking!
        '''
        self.sendQueue.put(message)

    def close(self):
        '''
        Close the connection
        Blocking!
        '''
        self.__cancelThread = True

        if self.__connThread != None:
            self.__connThread.join(5)  # 5 seconds
