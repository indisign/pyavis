'''

Option sets

Created on 04.03.2013

@author: berre
'''

from avis.legacy import LegacyConnectionOptions
from avis.security.keysets import Keys


class ConnectionOptions:
    '''
    Connection options sent by the client to the server.

    In addition to the options sent by the client to the router
    detailed below, the router may also add a "Vendor-Identification"
    option to its reply with a string identifying the router
    implementation, e.g. "Avis 1.2" or "elvind 4.4.0".

    <h2>Standard Elvin Connection Options</h2>

    <p>
    See http://elvin.org/specs for details on client connection
    options.
    </p>

    <dl>
    <dt>Packet.Max-Length</dt>
    <dd>Max packet length acceptable from a client.</dd>

    <dt>Subscription.Max-Count</dt>
    <dd>Maximum number of subscriptions allowed by a single client.</d

    <dt>Subscription.Max-Length</dt>
    <dd>Maximum length, in bytes, of any subscription expression.</dd>

    <dt>Receive-Queue.Max-Length</dt>
    <dd>The maximum size of the router's per-client incoming packet
    queue, in bytes. If the queue exceeds this size, the router will
    throttle the data stream from the client until the queue drops
    below this value.</dd>

    <dt>TCP.Send-Immediately</dt>
    <dd>Set whether the TCP NO_DELAY flag is enabled for sockets on
    the server side. 1 = send immediately (TCP NO_DELAY = true), 0 = d
    not necessarily send immediately, buffer data for optimal
    throughput (TCP NO_DELAY = false). Set this to 1 if you experience
    lag with "real time" applications that require minimal delivery
    latency, but note that this may result in an overall reduction in
    throughput.</dd>

    <dt>Attribute.Name.Max-Length</dt>
    <dd>The maximum length, in bytes, of an attribute name.</dd>

    <dt>Attribute.Max-Count</dt>
    <dd>The maximum number of attributes on a notification.</dd>

    <dt>Attribute.Opaque.Max-Length</dt>
    <dd>Maximum length, in bytes, for opaque values.</dd>

    <dt>Attribute.String.Max-Length</dt>
    <dd>Maximum length, in bytes, for opaque values. Note that this
    value is not the number of characters: some characters may take up
    to 5 bytes to represent using the required UTF-8 encoding.</dd>

    <dt>Receive-Queue.Drop-Policy</dt>
    <dd>This property describes the desired behaviour of the router's
    packet receive queue if it exceeds the negotitated maximum size.
    Values: "oldest", "newest", "largest", "fail"</dd>

    <dt>Send-Queue.Drop-Policy</dt>
    <dd>This property describes the desired behaviour of the router's
    packet send queue if it exceeds the negotitated maximum size.
    Values: "oldest", "newest", "largest", "fail"</dd>

    <dt>Send-Queue.Max-Length</dt>
    <dd>The maximum length (in bytes) of the routers send queue.</dd>
    </dl>
    '''

    values = {}
    includeLegacy = True
    legacyOpt = LegacyConnectionOptions()

    def asMapWithLegacy(self):
        '''
        Generate a dictionary view of this connection option set, automatically
        adding legacy connection options as required.
        '''
        if len(self.values) == 0:
            return {}

        options = {}
        for key in self.values.keys():

            if self.includeLegacy:
                value = self.values[key]

                # TCP.Send-Immediately maps to router.coalesce-delay which
                # has opposite meaning.
                if key == "TCP.Send-Immediately":
                    value = 1 if value == 0 else 0

                options[self.legacyOpt.newToLegacy(key)] = value

            options[key] = value

        return options

    def convertLegacyToNew(self, legacyOptions):
        '''
        Convert options that may contain legacy settings to new-style
        ones as required.
        '''
        if len(legacyOptions) == 0:
            return {}

        options = {}

        for key in legacyOptions.keys():
            value = legacyOptions[key]

            # router.coalesce-delay maps to TCP.Send-Immediately which has
            # opposite meaning.
            if key == "router.coalesce-delay":
                value = 1 if value == 0 else 0

            options[self.legacyOpt.legacyToNew(key)] = value

        return options

    def differenceFrom(self, options):
        '''
        Generate the difference between this option set and an actual set
        returned by the server.
        '''
        diff = {}

        for key in self.values.keys():
            actualValue = self.values[key]
            if options[key] != actualValue:
                diff[key] = actualValue

        return diff

    def set(self, name, value):
        '''
        Set a connection option.
        
        name: The option name.
        value: The value. Must be a string or a number. Use None to clear.
        '''

        if value != None and not isinstance(value, int) and not isinstance(value, str):
            raise TypeError("Value must be a string or integer.")

        self.values[name] = value

    def setAll(self, options):
        '''
        Set a number of options at once.
        
        options: a dictionary containing (name, value) pairs
        '''
        for key in options.keys():
            self.values[key] = options[key]

    def get(self, name):
        '''
        Get the value for a connection option, or None if not defined.
        '''
        return self.values[name]


class ElvinOptions:
    '''
    Options for configuring an Elvin client connection. Includes router
    connection parameters, security keys, SSL parameters and timeouts.

    The options object used to initialise the Elvin connection cannot
    be directly changed after the connection is created, but some
    settings can be changed on a live connection object using supported
    methods such as {@link Elvin#setNotificationKeys(Keys)}, etc.
    '''

    # The options sent to the router to negotiate connection
    # parameters. After connection, these will be updated to include
    # any extra values sent by the router, including the
    # Vendor-Identification option.
    connectionOptions = None

    # The global notification keys that apply to all notifications. 
    notificationKeys = Keys()

    # The global subscription keys that apply to all subscriptions.
    subscriptionKeys = Keys()

    # The (public) certificate used for TLS/SSL secure connections (i.e. connections via "elvin:/secure/..." URI's)
    # serverCertificate

    # Used to ensure that the router the client is connected to is
    # authentic. When true, only servers with a certificate
    # authenticated against the trusted certificates in the supplied
    # keystore will be acceptable for secure connections.
    requireAuthenticatedServer = False

    # The amount of time (in milliseconds) that must pass before the
    # router is assumed to not be responding to a request. Default is
    # 10 seconds.
    receiveTimeout = 10000

    # The liveness timeout period (in milliseconds). If no messages are
    # seen from the router in this period a connection test message is
    # sent and if no reply is seen the connection is deemed to be
    # defunct and automatically closed. Default is 60 seconds.
    livenessTimeout = 60000

    def __init__(self, connectionOptions, notificationKeys, subscriptionKeys):
        if connectionOptions == None: raise TypeError("The parameter connectionOptions may not be null.")
        if notificationKeys == None: raise TypeError("The parameter notificationKeys may not be null.")
        if subscriptionKeys == None: raise TypeError("The parameter subscriptionKeys may not be null.")

        self.connectionOptions = connectionOptions
        self.notificationKeys = notificationKeys
        self.subscriptionKeys = subscriptionKeys

    def updateConnectionOptions(self, options):
        '''
        Update connection options to include any new values from a given map.
        '''
        if len(options) > 0:
            self.connectionOptions.setAll(options)
