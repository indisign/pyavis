'''
Read/Write packets to streams according to the elvin protocol

Created on 04.03.2013

@author: berre
'''

from avis.exceptions import ProtocolCodecError
from avis.msg import *


def encodeMessage(message, outStream):
    '''
    Encode the given message to the output stream
    '''
    buf = io.BytesIO()

    # encode the message to temporary buffer (to get the length)
    message.encode(buf)

    # write the message header
    frameSize = buf.tell() + 4  # = typeId + data len
    bs = BinaryStream(outStream)
    bs.writeInt32(frameSize)
    bs.writeInt32(message.typeId())

    # message body
    buf.seek(0, io.SEEK_SET)
    bs.writeBytes(buf.getbuffer())

    # sanity check frame is 4-byte aligned
    if frameSize % 4 != 0:
        raise ProtocolCodecError("Frame length not 4 byte aligned for " + message.name());


def decodeMessage(inStream):
    '''
    Decode an elvin message from the given input stream
    '''
    bs = BinaryStream(inStream)
    frameSize = bs.readInt32()
    dataStart = inStream.tell()

    message = None

    try:
        messageType = bs.readInt32()
        message = newMessage(messageType, frameSize)

        if frameSize % 4 != 0:
            raise ProtocolCodecError("Frame length not 4 byte aligned")

        message.decode(inStream)

        bytesRead = inStream.tell() - dataStart

        if bytesRead != frameSize:
            raise ProtocolCodecError("Some input not read for " + message.name() + ": Frame header said " + str(
                frameSize) + " bytes, but only read " + str(bytesRead))

        return message

    except Exception as ex:
        error = ErrorMessage(ex, message)
        # fill in XID if possible
        if isinstance(message, XidMessage) and len(inStream) >= 12:
            inStream.seek(8, io.SEEK_SET)
            xid = bs.readInt32()
            if (xid > 0):
                message.xid = xid

        return error


def newMessage(messageType, frameSize):
    '''
    Get an empty instance of the message type identified by the given id
    '''
    if messageType == ConnRqst.ID:
        return ConnRqst()
    elif messageType == ConnRply.ID:
        return ConnRply()
    elif messageType == DisconnRqst.ID:
        return DisconnRqst()
    elif messageType == DisconnRply.ID:
        return DisconnRply()
    elif messageType == Disconn.ID:
        return Disconn()
    elif messageType == SubAddRqst.ID:
        return SubAddRqst()
    elif messageType == SubRply.ID:
        return SubRply()
    elif messageType == SubModRqst.ID:
        return SubModRqst()
    elif messageType == SubDelRqst.ID:
        return SubDelRqst()
    elif messageType == Nack.ID:
        return Nack()
    elif messageType == NotifyDeliver.ID:
        return NotifyDeliver()
    elif messageType == NotifyEmit.ID:
        return NotifyEmit()
    elif messageType == TestConn.ID:
        return TestConn()
    elif messageType == ConfConn.ID:
        return ConfConn()
    elif messageType == SecRqst.ID:
        return SecRqst()
    elif messageType == SecRply.ID:
        return SecRply()
    elif messageType == UNotify.ID:
        return UNotify()
    elif messageType == DropWarn.ID:
        return DropWarn()
    elif messageType == QuenchPlaceHolder.ADD or messageType == QuenchPlaceHolder.MODIFY or messageType == QuenchPlaceHolder.DELETE:
        qph = QuenchPlaceHolder()
        qph.messageType = messageType
        qph.length = frameSize - 4
        return qph

    raise ProtocolCodecError("Unknown message type: ID = " + str(messageType))
