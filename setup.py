from distutils.core import setup

setup(
    name='pyavis',
    version='1.0.0',
    packages=['avis', 'avis.utils', 'avis.client', 'avis.security', 'tests', 'examples'],
    url='',
    license='MIT',
    author='toga',
    author_email='toga@hrz.tu-chemnitz.de',
    description='Python Avis Event Router'
)
