'''
Tests for the protocol module

Created on 04.03.2013

@author: berre
'''

import io
from unittest import TestCase

from avis import protocol
from avis.msg import NotifyDeliver

'''
Tests
'''


def bigString():
    '''
    Create a big string
    '''
    s = ""
    for i in range(0, 800 * 1024):
        s += "A"

    return s


class EncodeDecodeTestCase(TestCase):
    def runTest(self):
        message = NotifyDeliver()
        message.attributes["string"] = bigString()
        message.attributes["blob"] = bytearray(1024 * 1024)

        stream = io.BytesIO()
        protocol.encodeMessage(message, stream)

        stream.seek(0, io.SEEK_SET)
        resMsg = protocol.decodeMessage(stream)

        assert resMsg.ID == NotifyDeliver.ID, 'Wrong message type'
        assert message.attributes["string"] == resMsg.attributes["string"], 'Incorrect decode/encode cycle'
        assert message.attributes["blob"] == resMsg.attributes["blob"], 'Incorrect decode/encode cycle'
