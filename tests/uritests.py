'''
Unit tests for the ElvinUri class

Created on 26.02.2013

@author: berre
'''

from unittest import TestCase

from avis import common
from avis.exceptions import UriFormatError
from avis.uri import ElvinUri

''' helper functions for the test cases '''


def assertInvalid(uriString):
    '''
    Throws an assertien exception if the given uriString
    is not detected as invalid by invoking the Elvinuri constructor
    '''
    try:
        ElvinUri(uriString)
        assert False, "Invalid URI \"" + uriString + "\" not detected"
    except UriFormatError:
        pass  # ok,  expected exception


def assertSameUri(uri1, uri2):
    '''
    Throw an assertion exception when the two URIs are not detected as equal
    '''
    e1 = ElvinUri(uri1)
    e2 = ElvinUri(uri2)
    assert hash(e1) == hash(e2), 'URIs hash codes are not equal'
    assert e1 == e2, 'Uri equality not detected'


def assertNotSameUri(uri1, uri2):
    '''
    Throw an assertion exception when the two URIs are detected as equal
    '''
    assert ElvinUri(uri1) != ElvinUri(uri2), 'URIs incorrectly detected as equal'


class VersionTestCase(TestCase):
    def runTest(self):
        e = ElvinUri("elvin://elvin_host")
        assert e.versionMajor == common.CLIENT_VERSION_MAJOR, 'Wrong default major version'
        assert e.versionMinor == common.CLIENT_VERSION_MINOR, 'Wrong default minor version'
        assert "elvin_host" == e.host, 'Incorrect host'

        e = ElvinUri("elvin:5.1//elvin_host")
        assert 5 == e.versionMajor, 'Wrong major version'
        assert 1 == e.versionMinor, 'Wrong minor version'
        assert "elvin_host" == e.host, 'Incorrect host'

        e = ElvinUri("elvin:5//elvin_host")
        assert 5 == e.versionMajor, 'Wrong major version'
        assert e.versionMinor == common.CLIENT_VERSION_MINOR, 'Wrong default minor version'
        assert "elvin_host" == e.host, 'Incorrect host'

        assertInvalid("http:hello//elvin_host");
        assertInvalid("elvin:hello//elvin_host");
        assertInvalid("elvin:4.0.0//elvin_host");
        assertInvalid("elvin:4.//elvin_host");
        assertInvalid("elvin: //elvin_host");
        # following not ported, since python ints (which are actually longs in python 3) are not directly bounded
        # assertInvalid("elvin:111111111111111.2222222222222222222//elvin_host"); 


class ProtocolTestCase(TestCase):
    def runTest(self):
        e = ElvinUri("elvin://elvin_host")
        assert common.DEFAULT_PROTOCOL == e.protocol, 'Wrong default protocol'

        e = ElvinUri("elvin:/tcp,xdr,ssl/elvin_host")
        assert e.protocol == ["tcp", "xdr", "ssl"], 'Wrong protocol'
        assert e.host == "elvin_host", 'Incorrect host'

        e = ElvinUri("elvin:/secure/elvin_host")
        assert common.SECURE_PROTOCOL == e.protocol, 'Wrong default secure protocol'

        assertInvalid("elvin:/abc,xyz/elvin_host");
        assertInvalid("elvin:/abc,xyz,dfg,qwe/elvin_host");
        assertInvalid("elvin:/abc,/elvin_host");
        assertInvalid("elvin:/,abc/elvin_host");
        assertInvalid("elvin:/abc,,xyz/elvin_host");
        assertInvalid("elvin:///elvin_host");


class EndpointTestCase(TestCase):
    def runTest(self):
        e = ElvinUri("elvin://elvin_host")
        assert "elvin_host" == e.host, 'Incorrect host'
        assert common.DEFAULT_PORT == e.port, 'Wrong default port'

        e = ElvinUri("elvin://elvin_host:12345")
        assert "elvin_host" == e.host, 'Incorrect host'
        assert e.port == 12345, 'Wrong port'

        assertInvalid("elvin://");
        assertInvalid("elvin://hello:there");


class OptionsTestCase(TestCase):
    def runTest(self):
        e = ElvinUri("elvin://elvin_host;name1=value1")
        d = {"name1": "value1"}
        assert e.options == d, 'Wrong options'

        e = ElvinUri("elvin://elvin_host;name1=value1;name2=value2")
        d = {"name1": "value1", "name2": "value2"}
        assert e.options == d

        assertInvalid("elvin://elvin_host;name1;name2=value2");
        assertInvalid("elvin://elvin_host;=name1;name2=value2");
        assertInvalid("elvin://elvin_host;");
        assertInvalid("elvin://;x=y");
        assertInvalid("elvin://;x=");


class EqualityTestCase(TestCase):
    def runTest(self):
        assertSameUri("elvin://elvin_host", "elvin://elvin_host:2917");
        assertSameUri("elvin://elvin_host", "elvin:/tcp,none,xdr/elvin_host");

        assertNotSameUri("elvin://elvin_host", "elvin:/tcp,ssl,xdr/elvin_host");
        assertNotSameUri("elvin://elvin_host", "elvin://elvin_host:29170");
        assertNotSameUri("elvin://elvin_host", "elvin://elvin_host;name=value");


class CanonicalizeTestCase(TestCase):
    def runTest(self):
        e = ElvinUri("elvin://elvin_host")
        assert "elvin://elvin_host" == str(e), 'Wrong string representation'
        assert "elvin:4.0/tcp,none,xdr/elvin_host:2917" == e.toCanonicalString(), 'Wrong canonical representation'

        e = ElvinUri("elvin://elvin_host;name1=value1")
        assert "elvin:4.0/tcp,none,xdr/elvin_host:2917;name1=value1" == e.toCanonicalString(), 'Wrong canonical representation'

        e = ElvinUri("elvin:/secure/elvin_host:29170;b=2;a=1")
        assert "elvin:4.0/ssl,none,xdr/elvin_host:29170;a=1;b=2" == e.toCanonicalString(), 'Wrong canonical representation'

        e = ElvinUri("elvin:5.1/secure/elvin_host:29170;b=2;a=1")
        assert "elvin:5.1/ssl,none,xdr/elvin_host:29170;a=1;b=2" == e.toCanonicalString(), 'Wrong canonical representation'


class Ipv6TestCase(TestCase):
    def runTest(self):
        '''
        Test IPv6 compatibility
        '''
        e = ElvinUri("elvin://[2001:0db8:85a3:08d3:1319:8a2e:0370:7344]:1234")
        assert e.host == "[2001:0db8:85a3:08d3:1319:8a2e:0370:7344]", 'Incorrect host'
        assert e.port == 1234, 'Wrong port'

        e = ElvinUri("elvin:/tcp,xdr,ssl/[::1/128]:4567")
        assert e.protocol == ["tcp", "xdr", "ssl"], 'Wrong protocol'
        assert e.host == "[::1/128]", "Incorrect host"
        assert e.port == 4567, 'Wrong port'

        assertInvalid("elvin://[::1/128")
        assertInvalid("elvin://[[::1/128")
        assertInvalid("elvin://[::1/128]]")
        assertInvalid("elvin://[]")
        assertInvalid("elvin://[")
        assertInvalid("elvin://[::1/128];hello")
        assertInvalid("elvin://[::1/128]:xyz")
        assertInvalid("elvin://[::1/128];")
        assertInvalid("elvin:///[::1/128]")
