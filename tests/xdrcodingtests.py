'''
Unit tests for the XdrCoding module

Created on 26.02.2013

@author: berre
'''

import io
from unittest import TestCase

from avis import xdrcoding

'''
Test utils
'''


def roundtrip(text):
    buf = io.BytesIO()
    xdrcoding.putString(buf, text)
    buf.seek(0, io.SEEK_SET)
    s = xdrcoding.getString(buf)
    assert text == s, "Roundtrip test produced wrong result"


'''
Tests
'''


class StringIOTestCase(TestCase):
    def runTest(self):
        buf = io.BytesIO()

        xdrcoding.putString(buf, "")
        assert 4 == buf.tell(), 'Wrong buffer position'

        buf.seek(0, io.SEEK_SET)
        xdrcoding.putString(buf, "hello")
        assert 12 == buf.tell(), 'Wrong buffer position'

        buf.seek(0, io.SEEK_SET)

        assert "hello" == xdrcoding.getString(buf), 'Wrong encoding/decoding'


class Utf8TestCase(TestCase):
    def runTest(self):
        roundtrip("Hello A\u0308\uFB03ns this is some bogus text")
        roundtrip("Hi there \u00C4\uFB03ns more bogus text");

        b = bytearray([0xc3, 0x94, 0xc3, 0xb8, 0xce, 0xa9, 0x73, 0x20, 0x73,
                       0x75, 0x70, 0x70, 0x6f, 0x73, 0x65, 0x20])

        roundtrip(str(b, "utf8"))


class NameValueIOTestCase(TestCase):
    def runTest(self):
        buf = io.BytesIO()
        nameValues = {}

        xdrcoding.putNameValues(buf, nameValues)
        assert 4 == buf.tell(), 'Wrong buffer position'

        buf.seek(0, io.SEEK_SET)
        nameValues["int"] = 42
        nameValues["opaque"] = bytearray([1, 2, 3])
        xdrcoding.putNameValues(buf, nameValues)
        assert 44 == buf.tell(), 'Wrong buffer position'

        buf.seek(0, io.SEEK_SET)
        res = xdrcoding.getNameValues(buf)
        assert len(nameValues) == len(res), 'Wrong length'
        assert nameValues["int"] == res["int"], 'Wrong key value pair'
        assert nameValues["opaque"] == res["opaque"], 'Wrong key value pair'


class ObjectsIOTestCase(TestCase):
    def runTest(self):
        buf = io.BytesIO()
        objects = ["hello", 42]

        xdrcoding.putObjects(buf, objects)
        buf.seek(0, io.SEEK_SET)
        objectsCopy = xdrcoding.getObjects(buf)

        assert objects == objectsCopy, 'Wrong deserialization'


class PaddingTestCase(TestCase):
    def runTest(self):
        assert 0 == xdrcoding.paddingFor(0), 'Wrong padding'
        assert 3 == xdrcoding.paddingFor(1), 'Wrong padding'
        assert 2 == xdrcoding.paddingFor(2), 'Wrong padding'
        assert 1 == xdrcoding.paddingFor(3), 'Wrong padding'
        assert 0 == xdrcoding.paddingFor(4), 'Wrong padding'
        assert 3 == xdrcoding.paddingFor(5), 'Wrong padding'
        assert 3 == xdrcoding.paddingFor(25), 'Wrong padding'
        assert 3 == xdrcoding.paddingFor(4 * 1234 + 1), 'Wrong padding'
