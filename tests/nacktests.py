'''
Unit tests for the Nack message

Created on 01.03.2013

@author: berre
'''

from unittest import TestCase

from avis.msg import Nack

'''
Tests
'''


class StringIOTestCase(TestCase):
    def runTest(self):
        nack = Nack()
        nack.args = ["foo", "bar"]
        nack.message = "There was a %1 in the %2 (%1, %2) %3 %"

        formattedMessage = nack.formattedMessage()
        assert formattedMessage == "There was a foo in the bar (foo, bar) %3 %", 'Wrong formatted message'
